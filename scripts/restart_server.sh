#!/bin/bash
sudo cp /opt/bitnami/apps/django/django_projects/SemantiCloud/conf/bitnami-apps-prefix.conf  /opt/bitnami/apache2/conf/bitnami/bitnami-apps-prefix.conf -rf
sudo pip install -r /opt/bitnami/apps/django/django_projects/SemantiCloud/requirements.txt
sudo chmod 777 -R /opt/bitnami/
sudo /opt/bitnami/ctlscript.sh restart apache