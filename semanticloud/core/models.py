from __future__ import unicode_literals
from django.db import models
from oauth2_provider.models import *
from semanticloud.models import CustomApplication


class Synset(models.Model):
    synsetCode = models.CharField(max_length=50, unique=True, db_index=True)
    definition = models.CharField(max_length=250)
    lexDomainId = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "Synsets"

    def __unicode__(self):
        return self.synsetCode


class Word(models.Model):
    lemma = models.CharField(max_length=100, unique=True, db_index=True)

    class Meta:
        verbose_name_plural = "Words"

    def __unicode__(self):
        return self.lemma


class Sense(models.Model):
    wordId = models.CharField(max_length=100, db_index=True)
    synsetId = models.CharField(max_length=50, db_index=True)
    language = models.CharField(max_length=3, db_index=True)
    removed = models.BooleanField(default=False)
    applicationRefID = models.ForeignKey(CustomApplication, null=True)

    class Meta:
        verbose_name_plural = "Senses"
        unique_together = ("wordId", "synsetId")


class Linktype(models.Model):
    linkid = models.IntegerField(primary_key=True)
    linkDescription = models.CharField(max_length=50, blank=True)
    recurses = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'linktypes'
        verbose_name_plural = "Link types (ONLY READ)"

    def __unicode__(self):
        return self.link


class Link(models.Model):
    synsetSId = models.CharField(max_length=50, db_index=True)
    synsetDId = models.CharField(max_length=50, db_index=True)
    applicationRefID = models.ForeignKey(CustomApplication, null=True)
    linkType = models.ForeignKey(Linktype)

    class Meta:
        verbose_name_plural = "Links"
