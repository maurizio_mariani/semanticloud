from django.http import HttpResponseNotAllowed, HttpResponseForbidden, HttpResponseRedirect

__author__ = 'maurizio'
from models import AccessToken
from django.conf import settings
from exceptions import LimitReached


def monthly_requests_safe(func):
    # try:
    def _decorator(request, *args, **kwargs):
        application = AccessToken.objects.get(token=request.META['HTTP_AUTHORIZATION'].split()[1]).application
        if application.monthly_requests < application.tier.monthly_request_limit:
            if settings.DEBUG is True:
                print "Safe request"
            return func(request, *args, **kwargs)
        else:
            if settings.DEBUG:
                print "Not safe request"
            return HttpResponseRedirect("RaiseForbidden")
    return _decorator
    # except LimitReached:
    #     raise LimitReached


