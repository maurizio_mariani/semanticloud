import base64
import json

# from django.contrib.sites.models import Site
from unittest import skip

import django
import os

from django.test import LiveServerTestCase

os.environ["DJANGO_SETTINGS_MODULE"] = "semanticloud.settings"
django.setup()

from django.test import TestCase, Client
import csv
import os
import time
from billing import get_gateway
from billing import CreditCard

_BASE_DIR = os.path.dirname(__file__)
_BASE_URL = 'http://localhost:8000/'
_BASE_URL_LIVE = 'http://localhost:8081/'
# _BASE_URL = Site.objects.get_current().domain


class TestAuthorizations(TestCase):
    def test_authorizations(self):
        """
        Requests should return thee status code specified
        """
        print _BASE_URL
        test_cases_dir = os.path.join(_BASE_DIR, 'test_cases')
        client = Client()
        with open(os.path.join(test_cases_dir, 'credentials.csv'), 'rb') as csvfile:
            spamreader = csv.DictReader(csvfile)
            for row in spamreader:
                payloads = {"grant_type": "client_credentials"}
                client_secret = row['ClientId'] + ':' + row['ClientSecret']
                r = client.post(_BASE_URL + "o/token/", payloads, **{'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(client_secret)})
                try:
                    print r
                    self.assertEquals(r.status_code, int(row['STATUS']))
                except AssertionError:
                    print "Failed at this row: "
                    print row
                    print r.status_code
                    raise

    def test_requests(self):
        """
        Requests should return thee status code defined
        """
        test_cases_dir = os.path.join(_BASE_DIR, 'test_cases')
        client = Client()
        out_file = open("test_log.txt", "wb")
        out_file.close()
        with open(os.path.join(test_cases_dir, 'credentials_for_requests_list.csv'), 'rb') as csvfile:
            spamreader = csv.DictReader(csvfile)
            for row in spamreader:
                payloads = {"grant_type": "client_credentials"}
                client_secret = row['ClientId'] + ':' + row['ClientSecret']
                url = _BASE_URL + "o/token/"
                r = client.post(url, payloads, **{'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(client_secret)})
                token = json.loads(r.content)['access_token']
                with open(os.path.join(test_cases_dir, 'requests_list.csv'), 'rb') as csvfilerequests:
                    spamreader_request = csv.DictReader(csvfilerequests)
                    out_file = open("test_log.txt", "wb")
                    for row_request in spamreader_request:
                        time_a = time.time()
                        url2 = _BASE_URL + "" + row_request['url']
                        r = client.get(url2, None, **{'HTTP_AUTHORIZATION': 'Bearer ' + token})
                        time_b = time.time()
                        out_file.write(row_request['url'] + " | " + str("%.2f" % (time_b - time_a)) + "\n")
                        try:
                            print "\n"
                            print row_request
                            try:
                                print r.streaming_content
                            except AttributeError:
                                try:
                                    print json.loads(r.content)
                                except ValueError:
                                    print r.content
                            self.assertEquals(r.status_code, int(row['status_code']))
                        except AssertionError:
                            print "Failed at this row: "
                            print row_request
                            print r.status_code
                            print "token:"
                            print token
                            raise
                    out_file.close()


class TestPayments(TestCase):
    def test_payment_success_in_debug(self):
        """
        Payment should return success
        """
        g1 = get_gateway("authorize_net")
        cc = CreditCard(first_name="Test", last_name="User", month=10, year=2020, number="4222222222222", verification_value="100")
        response1 = g1.purchase(1, cc)
        # response1 = g1.authorize(1, cc)
        try:
            self.assertEqual(response1['status'], 'SUCCESS')
        except AssertionError:
            print "Error status code is:" + str(response1['status'])
            print response1
            print response1['response'].response_code
            print response1['response'].response_reason_code
            print response1['response'].response_reason_text
            raise


class TestTryApiHome(TestCase):

    def test_try_api_refuse_if_no_token(self):
        """
        Try Api called without csrf token should return 403 error
        """
        client = Client(enforce_csrf_checks=True)
        url = "/try_api_home/"
        payloads = {"operation": "synonyms", "word": "sedia", "type": "n", "lang": "ita"}
        r = client.post(url, payloads)
        self.assertEqual(r.status_code, 403)

    # @skip("Need to test on frontend or provide valid csrf token")
    def test_try_api_return_status_code_200(self):
        """
        Try Api called without csrf token should return status code 200
        """
        client = Client()
        url = "/try_api_home/"
        payloads = {"operation": "synonyms", "word": "sedia", "type": "n", "lang": "ita"}
        r = client.post(url, json.dumps(payloads), content_type="application/json")
        self.assertEqual(r.status_code, 200)

