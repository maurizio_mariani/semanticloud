from django.conf import settings as custom_settings
import nltk
nltk.data.path.append(custom_settings.BASE_DIR)
from nltk.corpus import wordnet
from models import Sense, Link, Word, Synset

__author__ = 'maurizio'


class Languages:
    def __init__(self):
        pass

    ENGLISH = "eng"
    """ @var ENGLISH: parameter for the english language """
    ITALIAN = "ita"
    """ @var ITALIAN: parameter for the italian language """
    # languages = ['ita', 'eng']
    languages = sorted(wordnet.langs())


def get_synsets(word, word_type, language, application):
    """
    Return a list of synsets of the given word. Each synset is a tuple <id, code, definition>.\n
    The function will searching in ntlk wordnet before, then in bigflo wordnet if previous have no results.
    @param word:         word of which find synset
    @param word_type:    the type of given word ('n', 'v', 'None')
    @param language:     the language ('ENGLISH', 'ITALIAN')
    @param application:  application
    @return:             A list of synsets in the form: <id, code, definition>
    """
    results = []
    wntype = __get_word_type(word_type)
    my_synsets = []
    if language in Languages.languages:
            if wntype == 'Any':
                try:
                    if language == Languages.ENGLISH:
                        my_synsets = wordnet.synsets(word)
                    else:
                        my_synsets = wordnet.synsets(word, lang=language)
                except (RuntimeError, TypeError, NameError):
                    print ('Error 1')
                    print RuntimeError
                    print TypeError
                    print NameError
                    raise
            else:
                try:
                    if language == Languages.ENGLISH:
                        my_synsets = wordnet.synsets(word, pos=wntype)
                    else:
                        my_synsets = wordnet.synsets(word, pos=wntype, lang=language)
                except (RuntimeError, TypeError, NameError):
                    raise
    else:
        raise Exception("Error on language parameter. This exception was raised from utils.py")

    if len(my_synsets) > 0:
        if custom_settings.DEBUG:
            print "word found in nltk wordnet"
        for synset in my_synsets:
            results.append({"id": None, "code": str(synset.name()), "definition": str(synset.definition())})

    if custom_settings.CUSTOM_WORDNET is True:
        try:
            if custom_settings.DEBUG:
                print "Custom Wordnet Active"
            local_synsets = Sense.objects.filter(applicationRefID=application).filter(wordId=word)
            for local_synset in local_synsets:
                results.append({"id": None, "code": str(local_synset.synsetId), "definition": str(get_synset(local_synset.synsetId, application)["definition"])})
        except:
            pass
    return results


def meronyms(word, word_type, lang, application):
    """
    Return a list of meronyms of the given word.
    The function will searching in ntlk wordnet before, then in bigflo wordnet if previous have no results.
    @param word:         word of which find synonyms
    @param word_type:    the type of given word ('n', 'v', 'None')
    @param lang:         the language ('ENGLISH', 'ITALIAN')
    @param application:  the application
    @return:             A list of words
    """
    results = []
    wntype = __get_word_type(word_type)
    if lang in Languages.languages:
            my_synsets = wordnet.synsets(word, pos=wntype, lang=lang)
            if len(my_synsets) > 0:
                for synset in my_synsets:
                    meronyms = synset.part_meronyms()
                    for meronym in meronyms:
                        lemmas = get_lemmas(meronym.name(), lang, application)
                        for lemma in lemmas:
                            results.append(str(lemma))
                    if custom_settings.CUSTOM_WORDNET is True:
                        local_meronyms = Link.objects.filter(applicationRefID=application).filter(synsetDId=synset).filter(linkType=3)
                        for local_meronym in local_meronyms:
                            synset = get_synset(local_meronym.synsetSId, application)
                            lemmas = get_lemmas(synset, lang, application)
                            for lemma in lemmas:
                                results.append(lemma)
    else:
        raise Exception("Error on language parameter")
    results = list(set(results))
    return results


def more_general(word, word_type, lang, application):
    """
    Return the synset rapresenting the hypernyns of the given synset.
    The returned synset is a tuple in the form: <id, code, definition>.
    @param synset:       synset code of which find hypernyms
    @return:             A synset in the form <id, code, definition>
    """
    wntype = __get_word_type(word_type)
    results = []
    try:
        synsets = wordnet.synsets(word, wntype, lang)
        for synset in synsets:
            hypernyms = synset.hypernyms()
            for hypernym in hypernyms:
                lemmas = get_lemmas(hypernym.name(), lang, application)
                for lemma in lemmas:
                    results.append(lemma)
            if custom_settings.CUSTOM_WORDNET is True:
                local_hypernyms = Link.objects.filter(applicationRefID=application).filter(synsetDId=synset).filter(linkType=1)
                for local_hypernym in local_hypernyms:
                    synset = get_synset(local_hypernym.synsetSId, application)
                    lemmas = get_lemmas(synset, lang, application)
                    for lemma in lemmas:
                        results.append(lemma)
    except:
        raise
    results = list(set(results))
    return results


def more_specific(word, word_type, lang, application):
    """
    Return the synset rapresenting the hypernyns of the given synset.
    The returned synset is a tuple in the form: <id, code, definition>.
    @param synset:       synset code of which find hypernyms
    @return:             A synset in the form <id, code, definition>
    """
    wntype = __get_word_type(word_type)
    results = []
    synsets = wordnet.synsets(word, wntype, lang)
    for synset in synsets:
        hyponyms = synset.hyponyms()
        for hyponym in hyponyms:
            lemmas = get_lemmas(hyponym.name(), lang, application)
            for lemma in lemmas:
                results.append(lemma)

        local_hyponyms = Link.objects.filter(applicationRefID=application).filter(synsetDId=synset).filter(linkType=2)
        for local_hyponym in local_hyponyms:
            synset = get_synset(local_hyponym.synsetSId, application)
            lemmas = get_lemmas(synset, lang, application)
            for lemma in lemmas:
                results.append(lemma)
    results = list(set(results))
    return results


def get_word_syn(word, word_type, lang, application):
    """
    Return a list of synonyms of the given word.
    The function will searching in ntlk wordnet before, then in bigflo wordnet if previous have no results.
    @param word:         word of which find synonyms
    @param word_type:    the type of given word ('n', 'v', 'None')
    @param lang:         the language ('ENGLISH', 'ITALIAN')
    @return:             A list of words
    """
    results = []
    wntype = __get_word_type(word_type)
    if lang in Languages.languages:
            my_synsets = wordnet.synsets(word, pos=wntype, lang=lang)
            if len(my_synsets) > 0:
                for synset in my_synsets:
                    lemmas = get_lemmas(synset.name(), application)
                    for lemma in lemmas:
                        results.append(str(lemma.name()))
                results = list(set(results))
            else:
                raise Exception("Error word not found in nltk wordnet. This exception was raised from get_word_syn.utils.py")
    else:
        raise Exception("Error on language parameter")
    return results


def get_lemmas(synset, language, application):
    """
    Return a list of synonyms of the given synset.
    The function will searching in bigflo wordnet
    :param application:  application
    :param synset:       synset code of which find synonyms
    :param language:     language
    :return:             A list of words
    """
    results = []
    try:
        lemmas = wordnet.synset(synset).lemmas(language)
        for lemma in lemmas:
            results.append(str(lemma.name()))
    except:
        pass
    if custom_settings.CUSTOM_WORDNET is True:
        local_lemmas = Sense.objects.filter(applicationRefID=application).filter(synsetId=synset)
        for local_lemma in local_lemmas:
            results.append(str(local_lemma.wordId))
    return results


def get_hypernyms(synset, application):
    """
    Return the synset rapresenting the hypernyns of the given synset.
    The returned synset is a tuple in the form: <id, code, definition>.
    @param synset:       synset code of which find hypernyms
    @return:             A synset in the form <id, code, definition>
    """
    results = []
    try:
        hypernyms = wordnet.synset(synset).hypernyms()
        for hypernym in hypernyms:
            results.append({"id": 0, "code": str(hypernym.name()), "definition": str(hypernym.definition())})
    except:
        pass
    local_hypernyms = Link.objects.filter(applicationRefID=application).filter(synsetDId=synset).filter(linkType=1)
    for local_hypernym in local_hypernyms:
        results.append({"id": 0, "code": str(local_hypernym.synsetSId), "definition": str(get_synset(local_hypernym.synsetSId)["definition"])})
    return results


def get_r_hypernyms(synset, index, application):
    """
    Return the the list synset rapresenting the hyperyms recursive of the given synset.
    The returned synset are tuples in the form: <id, code, definition>.
    @param synset:       synset code of which find hypernyms
    @param index:        the index of recursion
    @return:             A list of synsets in the form <id, code, definition>
    """
    if int(index) > 10:
        local_index = 10
    else:
        local_index = index
    results = []
    for count in range(1, int(local_index) + 1):
        hypernyms = get_hypernyms(synset, application)
        for hypernym in hypernyms:
            results.append({"id": 0, "code": str(hypernym["code"]), "definition": str(hypernym["definition"])})
            synset = str(hypernym["code"])
    return results


def get_hyponyms(synset, application):
    """
    Return the the list synset rapresenting the hyponyms of the given synset.
    The returned synset are tuples in the form: <id, code, definition>.
    @param synset:       word of which find hyponyms
    @return:             A list of synsets in the form <id, code, definition>
    """
    results = []
    try:
        hyponyms = wordnet.synset(synset).hyponyms()
        for hyponym in hyponyms:
            results.append({"id": 0, "code": str(hyponym.name()), "definition": str(hyponym.definition())})
    except:
        pass
    local_hyponyms = Link.objects.filter(applicationRefID=application).filter(synsetDId=synset).filter(linkType=2)
    for local_hyponym in local_hyponyms:
        results.append({"id": 0, "code": str(local_hyponym.synsetSId), "definition": str(get_synset(local_hyponym.synsetSId)["definition"])})
    return results


def get_r_hyponyms(synset, index, application):
    """
    Return the the list synset rapresenting the hyponyms recursive of the given synset.
    The returned synset are tuples in the form: <id, code, definition>.
    @param synset:       synset code of which find hypernyms
    @param index:        the index of recursion
    @return:             A list of synsets in the form <id, code, definition>
    """
    if int(index) > 5:
        local_index = 5
    else:
        local_index = index
    results = []
    q = [{"code": synset, "level": 0}]
    while len(q) > 0:
        temp = q.pop()
        adj_list = get_hyponyms(temp['code'])
        if int(temp['level']) <= int(local_index):
            for element in adj_list:
                if element['code'] not in results:
                    if int(temp['level'] + 1) <= int(local_index):
                        code = element['code']
                        q.append({"code": code, "level": temp['level'] + 1})
                        results.append(({"id": 0, "code": str(code), "definition": str(element["definition"])}))
        else:
            return list(results)
    return list(results)


def get_homonyms(synset, application):
    """
    Return the the list synset rapresenting the homonyms of the given synset.
    The returned synset are tuples in the form: <id, code, definition>.
    @param synset:       synset code of which find homonyms
    @return:             A list of synsets in the form <id, code, definition>
    """
    list_parameters = synset.split(".")
    lemma = list_parameters[0]
    type = list_parameters[1]
    sensenum = list_parameters[2]
    results = []
    homonyms = get_synsets(lemma, type, Languages.ENGLISH, application)
    for homonym in homonyms:
        list_parameters_temp = homonym["code"].split(".")
        lemma_temp = list_parameters_temp[0]
        type_temp = list_parameters_temp[1]
        sensenum_temp = list_parameters_temp[2]
        if lemma != lemma_temp or type != type_temp or sensenum != sensenum_temp:
            results.append(homonym)
    return results


def get_antonyms(synset, application):
    """
    Return the the list synset rapresenting the antonyms of the given synset.
    The returned synset are tuples in the form: <id, code, definition>.
    @param synset:       synset code of which find antonyms
    @return:             A list of synsets in the form <id, code, definition>
    """
    results = []
    lemmas = get_lemmas(synset, application)
    for lemma in lemmas:
        wn_lemma = synset + "." + lemma
        try:
            antonyms = wordnet.lemma(wn_lemma).antonyms()
            for antonym in antonyms:
                results.append({"id": 0, "code": str(antonym.synset().name()), "definition": str(antonym.synset().definition())})
        except:
            raise Exception("Error word not found in nltk wordnet. This exception was raised from get_word_syn.utils.py")
    return results


def get_derivational_related_forms(synset, application):
    """
    Return the the list synset rapresenting the derivational related forms of the given synset.
    The returned synset are tuples in the form: <id, code, definition, link>.
    @param synset:       synset code of which find derivational related form
    @return:             A list of synsets in the form <id, code, definition, link>
    """
    results = []
    lemmas = get_lemmas(synset, application)
    for lemma in lemmas:
        wn_lemma = synset + "." + lemma
        try:
            derivationally = wordnet.lemma(wn_lemma).derivationally_related_forms()
            for derivation in derivationally:
                results.append({"id": 0, "code": str(derivation.synset().name()), "definition": str(derivation.synset().definition())})
        except:
            pass
    return results


def get_lex_domain(synset):
    """
    Return a word rapresenting the lexical domain.
    @param synset:       synset code of which find lexical domain
    @return:             A word rapresenting the lexical domain
    """
    lex = None
    try:
        lex = str(wordnet.synset(synset).lexname())
    except:
        pass
    return lex


def __get_word_type(word_type):
    if word_type == 'v':
        return 'v'
    elif word_type == 'n':
        return 'n'
    else:
        return 'a'
    # else:
    #     raise Exception("Improperly word type ('n'/'v'/'None'). This excaption is generated in utils.py")


def get_synset(synset, application):
    result = None
    synset_temp = None
    try:
        synset_temp = wordnet.synset(synset)
    except:
        pass
    if synset_temp:
        result = {"id": 0, "code": str(synset_temp.name()), "definition": str(synset_temp.definition())}
    else:
        local_synsets = Synset.objects.filter(applicationRefID=application).filter(synsetCode=synset)
        for local_synset in local_synsets:
            result = {"id": 0, "code": str(local_synset.synsetCode), "definition": str(local_synset.definition)}
    return result


def get_local_lemma(word):
    result = None
    lemmas = Word.objects.filter(lemma=word)
    for lemma in lemmas:
        result = lemma
    return result
