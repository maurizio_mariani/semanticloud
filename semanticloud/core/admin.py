from django.contrib import admin
from models import Synset, Sense, Word, Link


class SynsetAdmin(admin.ModelAdmin):
    list_display = ('synsetCode', 'definition', 'lexDomainId')
    search_fields = ['synsesCode']


class WordAdmin(admin.ModelAdmin):
    list_display = ('lemma',)
    search_fields = ['lemma']


class SenseAdmin(admin.ModelAdmin):
    list_display = ('wordId', 'synsetId', 'applicationRefID', 'removed')


class LinkAdmin(admin.ModelAdmin):
    list_display = ('synsetSId', 'synsetDId', 'linkType')

admin.site.register(Synset, SynsetAdmin)
admin.site.register(Word, WordAdmin)
admin.site.register(Sense, SenseAdmin)
admin.site.register(Link, LinkAdmin)
