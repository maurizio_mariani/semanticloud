__author__ = 'maurizio'

from django.conf.urls import url
import views


urlpatterns = [
    url(r'^get_lemmas/', views.get_lemmas_from_synset, name='get_lemmas'),
    url(r'^get_synonyms/$', views.get_synonyms, name='get_synonyms'),
    url(r'^get_part/$', views.get_meronyms, name='get_meronyms'),
    url(r'^get_more_general/$', views.get_more_general, name='get_more_general'),
    url(r'^get_more_specific/$', views.get_more_specific, name='get_more_specific'),
    url(r'get_lemmas_batch/', views.get_lemmas_from_synsets_batch),
    url(r'get_synsets/', views.get_synsets_from_word),
    url(r'get_synsets_batch/', views.get_synsets_from_words_batch),
    url(r'get_hypernyms/', views.get_hypernym),
    url(r'get_hypernyms_batch/', views.get_hypernym_batch),
    url(r'get_r_hypernyms/', views.get_r_hypernym),
    url(r'get_r_hypernyms_batch/', views.get_r_hypernym_batch),
    url(r'get_r_hyponyms/', views.get_r_hyponym),
    url(r'get_r_hyponyms_batch/', views.get_r_hyponym_batch),
    url(r'get_hyponyms/', views.get_hyponyms_view),
    url(r'get_hyponyms_batch/', views.get_hyponyms_view_batch),
    url(r'get_antonyms/', views.get_antonyms_view),
    url(r'get_antonyms_batch/', views.get_antonyms_view_batch),
    url(r'get_homonyms/', views.get_homonyms_view),
    url(r'get_homonyms_batch/', views.get_homonyms_view_batch),
    url(r'get_derivational/', views.get_derivational_view),
    url(r'get_derivational_batch/', views.get_derivational_view_batch),
    url(r'get_lex_domain/', views.get_lex_domains),
    url(r'get_lex_domain_batch/', views.get_lex_domains_batch),
    ]

