from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db.models import Q
from django.http import StreamingHttpResponse, JsonResponse, HttpResponseForbidden
import json
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.csrf import csrf_exempt
from models import Synset, Word, Sense
# from semanticloud.views import MyRequestValidator
from setuptools.package_index import unique_everseen
from utils import get_lemmas, get_synsets, Languages, get_r_hypernyms, get_r_hyponyms, get_hyponyms, get_antonyms, \
    get_homonyms, get_derivational_related_forms, get_lex_domain, more_general, more_specific, meronyms
from oauth2_provider.decorators import protected_resource, rw_protected_resource
from oauth2_provider.models import AccessToken
from decorators import monthly_requests_safe
from exceptions import LimitReached
from django.conf import settings

"""
@api {post} /o/token/                   Obtain the token
@apiName GetToken
@apiGroup 1.Token
@apiExample {curl} Example usage:
    curl -X POST -d "grant_type=client_credentials" -u"AKatMNE5WwVNnQC149qvs5K38O2bSVaQkd:HqSUqsliOfKbLxKYUGqsx3pYA9XhTjjlEI6jHVI0TKg7JHFmua3b48ZA9jCPbaEVDshOjRFulzzfUlk0IY7pGyglAMaWmJBKSETUBlj0UMsYsazTgv" /o/token/

@apiParam           {String}            grant_type=client_credentials       Default grant type
@apiParamExample    {String}            Request-Example:
        "grant_type=client_credentials"
@apiHeader          {String}            authorization                       Authorization token in the form "<client_id>:<client_secret>"
@apiHeaderExample   {String}            Header-Example:
        "AKatMNE5WwVNnQC149q5K38O2bSVaQkd:HqSUqsliOfKbmua3b48ZA9jCPbaEVDshOjRsIUBlj0UMsYsazTgv"

@apiSuccessExample Success-Response:
      HTTP/1.1 200 OK
      {
        "access_token": "mtEvT2AAtRkbFkVxHyb3T4JmGNWeSw",
        "token_type": "Bearer",
        "expires_in": 36000,
        "scope": "read write"
      }
@apiSuccess         {String}            access_token                        The access token given by the server
@apiSuccess         {String}            token_type                          Type of token
@apiSuccess         {String}            expires_in                          Expiration time
@apiSuccess         {String}            scope                               Scope of operations

@apiError           InvalidClient                                           The client is invalid

@apiErrorExample Error-Response:
      HTTP/1.1 401 InvalidClient
      {
        "error": "invalid_client"
      }
"""


@protected_resource()
def get_lemmas_from_synset(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_lemmas/?pass=4329ENqR&synset=crack.v.1
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False:
        return HttpResponseForbidden()
    else:
        synset = params.GET['synset']
        lemmas = get_lemmas(synset, application)
        data = json.dumps(lemmas)
        mimetype = 'application/json'
        return StreamingHttpResponse(data, mimetype)


@protected_resource()
def get_lemmas_from_synsets_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_lemmas_batch/?pass=4329ENqR&synsets=%5B%22crack.v.1%22%2C%20%22nut.n.1%22%5D
    """

    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False:
        return HttpResponseForbidden()
    else:
        data = []
        synsets = json.loads(params.GET['synsets'])
        for synset in synsets:
            lemmas = get_lemmas(synset, application)
            data.append({synset: lemmas})
        mimetype = 'application/json'
        return StreamingHttpResponse(json.dumps(data), mimetype)


@protected_resource()
def get_synsets_from_word(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_synsets/?pass=4329ENqR&word=crack
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        word = params.GET['word']
        try:
            type = params.GET['type']
        except MultiValueDictKeyError:
            type = 'None'
        synsets = get_synsets(word, type, Languages.ENGLISH, application)
        data = json.dumps(synsets)
        mimetype = 'application/json'
        return StreamingHttpResponse(data, mimetype)


"""
@api {get} /api/get_synonyms/                   Synonyms
@apiName GetSynonyms
@apiGroup 2.Api
@apiExample {curl} Example usage:
    curl -H "Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3" /api/get_synonyms/?word=chair&type=n&lang=eng

@apiParam           {String}            word                                Word you want to expand
@apiParam           {String}            type                                Verb, noun or adjective (v, n, a)
@apiParam           {String}            language                            Word you want to expand


@apiParamExample    {String}            Request-Example:
        word=chair&type=n&word=chair&lang=eng

@apiHeader          {String}            Authorization                       Authorization value in the form "Authorization: Bearer &lt;token&gt;"
@apiHeaderExample   {String}            Header-Example:
        "Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3"

@apiSuccessExample Success-Response:
      HTTP/1.1 200 OK
      [chair, professorship, president, chairman, chairwoman, chairperson, electric_chair, death_chair, hot_seat]
"""
@csrf_exempt
@protected_resource()
def get_synonyms(params):
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False:
        print 'limit_reached:'
        print _limit_reached(application)
        print 'tier_paid:'
        print _tier_paid(application)
        return HttpResponseForbidden()
    else:
        word = params.GET['word']
        try:
            type = params.GET['type']
        except MultiValueDictKeyError:
            type = 'None'
        lang = Languages.ENGLISH
        try:
            lang = params.GET['lang']
        except MultiValueDictKeyError:
            lang = Languages.ENGLISH
        synsets = get_synsets(word, type, lang, application)

        data = []
        for synset in synsets:
            print synset
            lemmas = get_lemmas(synset['code'], lang, application)
            for lemma in lemmas:
                try:
                    sense = Sense.objects.get(Q(applicationRefID=application), Q(synsetId=synset['code']), Q(wordId=lemma), Q(language=lang))
                    if sense.removed is False:
                        data.append(lemma)
                except ObjectDoesNotExist:
                    data.append(lemma)
                except MultipleObjectsReturned:
                    raise
        data = list(unique_everseen(data))
        data = json.dumps(data)
        _increment_monthly_requests(application)
        return JsonResponse(data, safe=False)


"""
@api {get} /api/get_more_general/                   More general
@apiName GetMoreGeneral
@apiGroup 2.Api
@apiExample {curl} Example usage:
    curl -H "Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3" /api/get_more_general/?word=chair&type=n&lang=eng

@apiParam           {String}            word                                Word you want to expand
@apiParam           {String}            type                                Verb, noun or adjective (v, n, a)
@apiParam           {String}            language                            Word you want to expand


@apiParamExample    {String}            Request-Example:
        word=chair&type=n&word=chair&lang=eng

@apiHeader          {String}            Authorization                       Authorization value in the form "Authorization: Bearer &lt;token&gt;"
@apiHeaderExample   {String}            Header-Example:
        "Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3"

@apiSuccessExample Success-Response:
      HTTP/1.1 200 OK
      [berth, office, instrument_of_execution, billet, spot, seat, place, situation, position, post, presiding_officer]
"""
@csrf_exempt
@protected_resource()
def get_more_general(params):
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        word = params.GET['word']
        try:
            type = params.GET['type']
        except MultiValueDictKeyError:
            type = 'None'
        lang = Languages.ENGLISH
        try:
            lang = params.GET['lang']
        except MultiValueDictKeyError:
            lang = Languages.ENGLISH
        data = []
        generals = more_general(word, type, lang, application)
        data = list(unique_everseen(generals))
        data = json.dumps(data)
        _increment_monthly_requests(application)
        return JsonResponse(data, safe=False)


"""
@api {get} /api/get_more_specific/                   More specific
@apiName GetMoreSpecific
@apiGroup 2.Api
@apiExample {curl} Example usage:
    curl -H "Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3" /api/get_more_specific/?word=chair&type=n&lang=eng

@apiParam           {String}            word                                Word you want to expand
@apiParam           {String}            type                                Verb, noun or adjective (v, n, a)
@apiParam           {String}            language                            Word you want to expand


@apiParamExample    {String}            Request-Example:
        word=chair&type=n&word=chair&lang=eng

@apiHeader          {String}            Authorization                       Authorization value in the form "Authorization: Bearer &lt;token&gt;"
@apiHeaderExample   {String}            Header-Example:
        "Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3"

@apiSuccessExample Success-Response:
      HTTP/1.1 200 OK
      [rocker, daybed, armchair, highchair, tablet-armed_chair, straight_chair, ladder-back, ladder-back_chair, garden_chair, side_chair, chaise_longue, chaise, barber_chair, chair_of_state, Eames_chair, Kalon_Tripa, vice_chairman, swivel_chair, folding_chair, wheelchair, feeding_chair, lawn_chair, fighting_chair]
"""
@csrf_exempt
@protected_resource()
def get_more_specific(params):
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        word = params.GET['word']
        try:
            type = params.GET['type']
        except MultiValueDictKeyError:
            type = 'None'
        lang = Languages.ENGLISH
        try:
            lang = params.GET['lang']
        except MultiValueDictKeyError:
            lang = Languages.ENGLISH
        data = []
        specifics = more_specific(word, type, lang, application)
        data = list(unique_everseen(specifics))
        data = json.dumps(data)
        _increment_monthly_requests(application)
        return JsonResponse(data, safe=False)


"""
@api {get} /api/get_part/                   Part
@apiName GetPart
@apiGroup 2.Api
@apiExample {curl} Example usage:
    curl -H "Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3" /api/get_part/?word=chair&type=n&lang=eng

@apiParam           {String}            word                                Word you want to expand
@apiParam           {String}            type                                Verb, noun or adjective (v, n, a)
@apiParam           {String}            language                            Word you want to expand


@apiParamExample    {String}            Request-Example:
        word=chair&type=n&word=chair&lang=eng

@apiHeader          {String}            Authorization                       Authorization value in the form "Authorization: Bearer &lt;token&gt;"
@apiHeaderExample   {String}            Header-Example:
        "Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3"

@apiSuccessExample Success-Response:
      HTTP/1.1 200 OK
      [leg, back, backrest]
"""
@csrf_exempt
@protected_resource()
def get_meronyms(params):
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False:
        return HttpResponseForbidden()
    else:
        word = params.GET['word']
        try:
            type = params.GET['type']
        except MultiValueDictKeyError:
            type = 'None'
        data = []
        lang = Languages.ENGLISH
        try:
            lang = params.GET['lang']
        except MultiValueDictKeyError:
            lang = Languages.ENGLISH
        specifics = meronyms(word, type, lang, application)
        data = list(unique_everseen(specifics))
        data = json.dumps(data)
        _increment_monthly_requests(application)
        return JsonResponse(data, safe=False)


@csrf_exempt
@protected_resource()
def get_synsets_from_words_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_synsets_batch/?pass=4329ENqR&words=%5B%7B%22word%22%3A%20%22crack%22%2C%20%22type%22%3A%20%22v%22%7D%2C%20%7B%22word%22%3A%20%22nut%22%2C%20%22type%22%3A%20%22n%22%7D%5D
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        data = []
        words = json.loads(params.GET['words'])
        for word_tuple in words:
            try:
                type = word_tuple['type']
            except MultiValueDictKeyError:
                type = 'None'
            synsets = get_synsets(word_tuple['word'], type, Languages.ENGLISH, application)
            data.append({word_tuple['word']: synsets})
        mimetype = 'application/json'
        return StreamingHttpResponse(json.dumps(data), mimetype)


@csrf_exempt
@protected_resource()
def get_hypernym(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_hypernyms/?pass=4329ENqR&synset=nut.n.1
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        synset = params.GET['synset']
        hypernym = get_r_hypernyms(synset, 1, application)
        json_result = json.dumps(hypernym)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_hypernym_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet
    /get_hypernyms_batch/?pass=4329ENqR&synsets=%5B%22crack.v.01%22%2C%20%22nut.n.01%22%5D
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        data = []
        synsets = json.loads(params.GET['synsets'])
        for synset in synsets:
            hypernym = get_r_hypernyms(synset, 1, application)
            data.append({synset: hypernym})
        json_result = json.dumps(data)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_r_hypernym(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_r_hypernyms/?pass=4329ENqR&synset=nut.n.1&index=2
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        synset = params.GET['synset']
        index = params.GET['index']
        hypernym = get_r_hypernyms(synset, index, application)
        json_result = json.dumps(hypernym)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_r_hypernym_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_r_hypernyms_batch/?pass=4329ENqR&synsets=%5B%7B%22synset%22%3A%20%22crack.v.01%22%2C%20%22index%22%3A%201%7D%2C%20%7B%22synset%22%3A%20%22nut.n.01%22%2C%20%22index%22%3A%202%7D%5D
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        data = []
        synsets = json.loads(params.GET['synsets'])
        for synset_tuple in synsets:
            synset = synset_tuple['synset']
            index = synset_tuple['index']
            hypernym = get_r_hypernyms(synset, index, application)
            data.append({synset: hypernym})
        json_result = json.dumps(data)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_r_hyponym(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_r_hyponyms/?pass=4329ENqR&synset=nut.n.1&index=2
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        synset = params.GET['synset']
        index = params.GET['index']
        hyponyms = get_r_hyponyms(synset, index, application)
        json_result = json.dumps(hyponyms)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_r_hyponym_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_r_hyponyms_batch/?pass=4329ENqR&synsets=%5B%7B%22synset%22%3A%20%22crack.v.01%22%2C%20%22index%22%3A%201%7D%2C%20%7B%22synset%22%3A%20%22nut.n.01%22%2C%20%22index%22%3A%202%7D%5D
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        data = []
        synsets = json.loads(params.GET['synsets'])
        for synset_tuple in synsets:
            index = synset_tuple['index']
            synset = synset_tuple['synset']
            hyponyms = get_r_hyponyms(synset, index, application)
            data.append({synset: hyponyms})
        json_result = json.dumps(data)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_hyponyms_view(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_hyponyms/?pass=4329ENqR&synset=nut.n.1
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        synset = params.GET['synset']
        hyponyms = get_hyponyms(synset, application)
        json_result = json.dumps(hyponyms)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_hyponyms_view_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_hyponyms_batch/?pass=4329ENqR&synsets=%5B%22crack.v.01%22%2C%20%22nut.n.01%22%5D
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        data = []
        synsets = json.loads(params.GET['synsets'])
        for synset in synsets:
            hyponyms = get_hyponyms(synset, application)
            data.append({synset: hyponyms})
        json_result = json.dumps(data)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_antonyms_view(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_antonyms/?pass=4329ENqR&synset=nut.n.1
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True and _tier_paid(application) is False:
        return HttpResponseForbidden()
    else:
        synset = params.GET['synset']
        antonyms = get_antonyms(synset, application)
        json_result = json.dumps(antonyms)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_antonyms_view_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_antonyms_batch/?pass=4329ENqR&synsets=%5B%22open.v.01%22%2C%20%22nut.n.01%22%5D
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        data = []
        synsets = json.loads(params.GET['synsets'])
        for synset in synsets:
            antonyms = get_antonyms(synset, application)
            data.append({synset: antonyms})
        json_result = json.dumps(data)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_homonyms_view(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_homonyms/?pass=4329ENqR&synset=nut.n.1
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        synset = params.GET['synset']
        homonyms = get_homonyms(synset)
        json_result = json.dumps(homonyms, application)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_homonyms_view_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_homonyms_batch/?pass=4329ENqR&synsets=%5B%22crack.v.01%22%2C%20%22nut.n.01%22%5D
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        data = []
        synsets = json.loads(params.GET['synsets'])
        for synset in synsets:
            homonyms = get_homonyms(synset, application)
            data.append({synset: homonyms})
        json_result = json.dumps(data)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_derivational_view(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_derivational/?pass=4329ENqR&synset=nut.n.1
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        synset = params.GET['synset']
        derivational = get_derivational_related_forms(synset, application)
        json_result = json.dumps(derivational)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_derivational_view_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_derivational_batch/?pass=4329ENqR&synsets=%5B%22crack.v.01%22%2C%20%22nut.n.01%22%5D
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        data = []
        synsets = json.loads(params.GET['synsets'])
        for synset in synsets:
            derivational = get_derivational_related_forms(synset, application)
            data.append({synset: derivational})
        json_result = json.dumps(data)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_lex_domains(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_lex_domain/?pass=4329ENqR&synset=nut.n.01
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        synset = params.GET['synset']
        lex_domain = get_lex_domain(synset, application)
        json_result = json.dumps(lex_domain)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


@csrf_exempt
@protected_resource()
def get_lex_domains_batch(params):
    """
    http://bigflowordnetvm.cloudapp.net/bigflowordnet/get_lex_domain_batch/?pass=4329ENqR&synsets=%5B%22crack.v.01%22%2C%20%22nut.n.01%22%5D
    """
    application = AccessToken.objects.get(token=params.META['HTTP_AUTHORIZATION'].split()[1]).application
    if _limit_reached(application) is True or _tier_paid(application) is False: 
        return HttpResponseForbidden()
    else:
        data = []
        synsets = json.loads(params.GET['synsets'])
        for synset in synsets:
            lex_domain = get_lex_domain(synset, application)
            data.append({synset: lex_domain})
        json_result = json.dumps(data)
        mimetype = 'application/json'
        return StreamingHttpResponse(json_result, mimetype)


def check_local_synset(request):
    try:
        Synset.objects.get(synsetCode=request.GET.get('synset'))
        return StreamingHttpResponse(True)
    except:
        return StreamingHttpResponse(False)


def check_local_lemma(request):
    try:
        Word.objects.get(lemma=request.GET.get('lemma'))
        return StreamingHttpResponse(True)
    except:
        return StreamingHttpResponse(False)


def _increment_monthly_requests(application):
    application.monthly_requests += 1
    application.save()


def _limit_reached(application):
    if application.monthly_requests < application.tier.monthly_request_limit:
        return False
    elif application.tier.monthly_request_limit == -1:
        return False
    else:
        return True


def _tier_paid(application):
    if application.tier.name == 'Free':
        return True
    elif application.tier_paid is not None:
        return True
    else:
        if settings.BETA:
            return True
        else:
            return False




