/**
 * Created by maurizio on 17/08/16.
 */
(function(){
    semantiCloud.directive('navbarHeader', ['$sce', function($sce){
        return {
            restrict: "E",
            scope: {
                showLogo: '=showLogo',
                page:"=page"
            },
            link: function(element, attributes, scope){
                scope.showIt = function(page){
                    return page==scope.page;
                }
            },
            controller: function($scope) {
                $scope.user_auth = userInfo;
                console.log($scope.user_auth);
                console.log("user.id == None");
                console.log($scope.user_auth.id == 'None');
                $scope.userLogged = function(){
                    return $scope.user_auth.id == 'None';
                };
            },
            templateUrl: $sce.trustAsResourceUrl("templates/navbar-header.html")
        };
    }]);
})();