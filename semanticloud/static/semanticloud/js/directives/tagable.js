(function(){
    semantiCloud.directive('tagable', function(){
        return {
            restrict: "A",
            link: function(element, attributes, scope){
                console.log(attributes[0].id);
                element.tagit();
            }
        };
    });
})();