/**
 * Created by maurizio on 19/08/16.
 */
(function(){
    semantiCloud.factory('languagesService', [function(){
        var LANGUAGES = [{0:"al", 1:"als"},{0:"ar", 1:"arb"},{0:"bu", 1:"bul"},{0:"ca", 1:"cat"},{0:"cm", 1:"cmn"},{0:"da", 1:"dan"},{0:"el", 1:"ell"},{0:"en", 1:"eng"},{0:"eu", 1:"eus"},{0:"fa", 1:"fas"},{0:"fi", 1:"fin"},{0:"fr", 1:"fra"},{0:"gl",1:"glg"},{0:"he",1:"heb"},{0:"hr",1:"hrv"},{0:"in",1:"ind"},{0:"it",1:"ita"},{0:"jp",1:"jpn"},{0:"nn",1:"nno"},{0:"no",1:"nob"},{0:"po",1:"pol"},{0:"po", 1:"por"},{0:"qc",1:"qcn"},{0:"sl", 1:"slv"},{0:"sp", 1:"spa"},{0:"sw", 1:"swe"},{0:"th", 1:"tha"},{0:"zs", 1:"zsm"}];
        this.get_languages = function(lang){
            if (lang!=undefined) {
                var language = $.grep(LANGUAGES, function(e){ return e[0] == lang; });
                console.log(language[0][1]);
                return language[0][1]
            } else {
                return LANGUAGES;
            }
        };
        return this;
    }]);
})();