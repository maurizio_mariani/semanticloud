/**
 * Created by maurizio on 18/02/16.
 */

(function(){
    semantiCloud = angular.module('semantiCloud', ['ngTagsInput']);
    semantiCloud.config(['$httpProvider', function(provider){
        provider.defaults.xsrfCookieName = 'csrftoken';
        provider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }]);
})();


