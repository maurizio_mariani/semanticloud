/**
 * Created by maurizio on 18/02/16.
 */
(function(){
    semantiCloud.controller('applicationsController',['$scope', '$http', 'languagesService', function($scope, $http, languagesService){
        var self = this;
        // var LANGUAGES = [{0:"al", 1:"als"},{0:"ar", 1:"arb"},{0:"bu", 1:"bul"},{0:"ca", 1:"cat"},{0:"cm", 1:"cmn"},{0:"da", 1:"dan"},{0:"el", 1:"ell"},{0:"en", 1:"eng"},{0:"eu", 1:"eus"},{0:"fa", 1:"fas"},{0:"fi", 1:"fin"},{0:"fr", 1:"fra"},{0:"gl",1:"glg"},{0:"he",1:"heb"},{0:"hr",1:"hrv"},{0:"in",1:"ind"},{0:"it",1:"ita"},{0:"jp",1:"jpn"},{0:"nn",1:"nno"},{0:"no",1:"nob"},{0:"po",1:"pol"},{0:"po", 1:"por"},{0:"qc",1:"qcn"},{0:"sl", 1:"slv"},{0:"sp", 1:"spa"},{0:"sw", 1:"swe"},{0:"th", 1:"tha"},{0:"zs", 1:"zsm"}];
        var applications = application_list;
        var BETA_OBJ = BETA;
        console.log("BETA HERE");
        console.log(BETA_OBJ);

        var relations_to_add = {
            'synonym': 'synonym',
            'more_general': 'more general',
            'more_specific': 'more specific',
            'part_of': 'part of'
        };
        this.languagesService = languagesService;
        this.arraysEqual = function(app){
            return _.isEqual(app.list_of_results, app.list_of_results_changed);
        };

        this.get_relation_to_add = function(app){
            return relations_to_add[app.relation_to_add_key];
        };

        this.dismiss_collapse_panel = function(client_id){
            $("#collapse_add_words_" + client_id).collapse('toggle');
        };

        this.set_relation_to_add = function(app, relation){
            if (!$("#collapse_add_words_" + app.client_id).hasClass("in")){
                $("#collapse_add_words_" + app.client_id).collapse('toggle');
            }
            app.relation_to_add_key = relation;
        };

         this.is_beta = function() {
            return BETA_OBJ.val=='True';
        };

        // this.get_languages = function(lang){
        //     var support_array = LANGUAGES.slice();
        //     support_array.splice(support_array.indexOf(lang), 1);
        //     return support_array;
        // };

        // this.get_languages = function(lang){
        //     if (lang!=undefined) {
        //         var language = $.grep(LANGUAGES, function(e){ return e[0] == lang; });
        //         console.log(language[0][1]);
        //         return language[0][1]
        //     } else {
        //         return LANGUAGES;
        //     }
        // };

        this.get_application_list = function(){
            return applications;
        };

        this.get_percent = function(app){
            return ((parseInt(app.monthly_requests))*100)/parseInt(app.tier.monthly_requests_limit);
        };

        this.goToPayment = function(appId, tierId){
            $http({

            }).then(function (response){

            })
        };

        this.try_api = function(application){
            //console.log(application);
            $scope.$watch(
                function() { return application.word; },
                function ( newValue, oldValue ) {
                    application.list_of_results.length = 0;
                    application.list_of_results_changed.length = 0;
                }, true
            );
            $scope.$watch(
                function() { return application.type; },
                function ( newValue, oldValue ) {
                    application.list_of_results.length = 0;
                    application.list_of_results_changed.length = 0;
                }, true
            );
            $scope.$watch(
                function() { return application.operation; },
                function ( newValue, oldValue ) {
                    application.list_of_results.length = 0;
                    application.list_of_results_changed.length = 0;
                }, true
            );
             $scope.$watch(
                function() { return application.language; },
                function ( newValue, oldValue ) {
                    application.list_of_results.length = 0;
                    application.list_of_results_changed.length = 0;
                }, true
            );
            var csrftoken = Cookies.get('csrftoken');
            var ladda = Ladda.create( document.querySelector( '.ladda_bind_' + application.client_id) );
            ladda.start();
            $http({
                method : "GET",
                url : "/try_api/",
                params: {
                    'client_id': application.client_id,
                    'client_secret': application.client_secret,
                    'operation': application.operation,
                    'word': application.word,
                    'type': application.type,
                    'lang': self.languagesService.get_languages(application.language),
                    // 'csrfmiddlewaretoken': csrftoken
                }
            }).then(function mySucces(response) {
                var data = jQuery.parseJSON(response.data);
                application.monthly_requests ++;
                data = jQuery.parseJSON(data);
                $.each(data, function(key, val){
                    application.list_of_results.push({"text": val});
                    application.list_of_results_changed.push({"text": val});
                });
                ladda.stop();
            }, function myError(response) {
                alert("Error");
                ladda.stop();
            });
        };

        this.update_semantic = function (application) {
            bootbox.confirm("Do you want to update semantic?", function(result) {
                if (result==true) {
                    var ladda = Ladda.create( document.querySelector( '.ladda_bind_modify_' + application.client_id) );
                    ladda.start();
                    var csrftoken = Cookies.get('csrftoken');
                    $http({
                        method : "POST",
                        url : "/update_sematic/",
                        params: {
                            'application_id': application.id,
                            'client_id': application.client_id,
                            'word': application.word,
                            'word_list': JSON.stringify(application.list_of_results),
                            'semantic': application.operation,
                            'word_type': application['type'],
                            'language': self.languagesService.get_languages(application.language)
                        }
                    }).then(function mySucces(response) {
                        console.log(response);
                        response.data.forEach(function (element) {
                            
                        });
                        ladda.stop();
                    }, function myError(response) {
                        alert("Error");
                        console.log(response);
                        ladda.stop();
                    });
                }
            });

        };
    }])
})();