#!/usr/bin/env bash
source ../env/bin/activate
apidoc -i core/ -f views.py -o semanticloud/static/doc_template/ -t doc_template/
cp -f ./doc_template/index.html semanticloud/templates/doc_template/index.html
#cp semanticloud/semanticloud/static/doc_template/js/main_permanent.js semanticloud/semanticloud/static/doc_template/js/main.js
python manage.py collectstatic --noinput