# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-19 17:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('semanticloud', '0001_squashed_0017_auto_20160225_1826'),
    ]

    operations = [
        migrations.AddField(
            model_name='customapplication',
            name='auto_renewal',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='customapplication',
            name='tier_paid',
            field=models.BooleanField(default=False, editable=False),
        ),
    ]
