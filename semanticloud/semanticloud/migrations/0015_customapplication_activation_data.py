# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-25 18:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('semanticloud', '0014_remove_customapplication_activation_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='customapplication',
            name='activation_data',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
