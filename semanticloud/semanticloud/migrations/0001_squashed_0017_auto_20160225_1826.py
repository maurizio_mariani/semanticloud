# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-25 19:44
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import oauth2_provider.generators
import oauth2_provider.validators


class Migration(migrations.Migration):

    replaces = [(b'semanticloud', '0001_initial'), (b'semanticloud', '0002_customapplication_agree'), (b'semanticloud', '0003_auto_20160216_2326'), (b'semanticloud', '0004_auto_20160216_2341'), (b'semanticloud', '0005_auto_20160217_2102'), (b'semanticloud', '0006_auto_20160217_2109'), (b'semanticloud', '0007_remove_customapplication_activation_data'), (b'semanticloud', '0008_customapplication_activation_data'), (b'semanticloud', '0009_customapplication_tier'), (b'semanticloud', '0010_auto_20160218_1433'), (b'semanticloud', '0011_auto_20160225_1407'), (b'semanticloud', '0012_auto_20160225_1444'), (b'semanticloud', '0013_auto_20160225_1818'), (b'semanticloud', '0014_remove_customapplication_activation_data'), (b'semanticloud', '0015_customapplication_activation_data'), (b'semanticloud', '0016_auto_20160225_1825'), (b'semanticloud', '0017_auto_20160225_1826')]

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomApplication',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('client_id', models.CharField(db_index=True, default=oauth2_provider.generators.generate_client_id, max_length=100, unique=True)),
                ('redirect_uris', models.TextField(blank=True, help_text='Allowed URIs list, space separated', validators=[oauth2_provider.validators.validate_uris])),
                ('client_type', models.CharField(choices=[('confidential', 'Confidential'), ('public', 'Public')], max_length=32)),
                ('authorization_grant_type', models.CharField(choices=[(b'client-credentials', 'Client credentials')], max_length=32)),
                ('client_secret', models.CharField(blank=True, db_index=True, default=oauth2_provider.generators.generate_client_secret, max_length=255)),
                ('name', models.CharField(blank=True, max_length=255)),
                ('skip_authorization', models.BooleanField(default=False)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='semanticloud_customapplication', to=settings.AUTH_USER_MODEL)),
                ('agree', models.BooleanField(default=None)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterModelOptions(
            name='customapplication',
            options={'verbose_name_plural': 'Custom Applications'},
        ),
        migrations.RemoveField(
            model_name='customapplication',
            name='agree',
        ),
        migrations.AlterField(
            model_name='customapplication',
            name='authorization_grant_type',
            field=models.CharField(choices=[(b'client-credentials', 'Client credentials')], default=b'client-credentials', max_length=32),
        ),
        migrations.CreateModel(
            name='Tier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('description', models.TextField(max_length=3000)),
                ('monthly_request_limit', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='customapplication',
            name='activation_data',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='customapplication',
            name='tier',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to='semanticloud.Tier'),
        ),
        migrations.AddField(
            model_name='customapplication',
            name='monthly_requests',
            field=models.IntegerField(default=0),
        ),
        migrations.CreateModel(
            name='CustomApplicationProxy',
            fields=[
            ],
            options={
                'proxy': True,
                'verbose_name_plural': 'Custom Applications Proxy',
            },
            bases=('semanticloud.customapplication',),
        ),
        migrations.AlterField(
            model_name='customapplication',
            name='client_id',
            field=models.CharField(db_index=True, default=oauth2_provider.generators.generate_client_id, editable=False, max_length=100, unique=True),
        ),
        migrations.AlterField(
            model_name='customapplication',
            name='client_secret',
            field=models.CharField(blank=True, db_index=True, default=oauth2_provider.generators.generate_client_secret, editable=False, max_length=255),
        ),
        migrations.AlterField(
            model_name='customapplication',
            name='monthly_requests',
            field=models.IntegerField(default=0, editable=False),
        ),
        migrations.AlterField(
            model_name='customapplication',
            name='tier',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='semanticloud.Tier'),
        ),
        migrations.AlterField(
            model_name='customapplication',
            name='user',
            field=models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE, related_name='semanticloud_customapplication', to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='CustomApplicationProxy',
        ),
        migrations.AlterField(
            model_name='customapplication',
            name='tier',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to='semanticloud.Tier'),
        ),
        migrations.AlterModelOptions(
            name='customapplication',
            options={},
        ),
        migrations.RemoveField(
            model_name='customapplication',
            name='tier',
        ),
        migrations.RemoveField(
            model_name='customapplication',
            name='activation_data',
        ),
        migrations.AddField(
            model_name='customapplication',
            name='activation_data',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.RemoveField(
            model_name='customapplication',
            name='monthly_requests',
        ),
        migrations.AddField(
            model_name='customapplication',
            name='monthly_requests',
            field=models.IntegerField(default=0, editable=False),
        ),
        migrations.AddField(
            model_name='customapplication',
            name='tier',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='semanticloud.Tier'),
        ),
    ]
