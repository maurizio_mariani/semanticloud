from django.core.urlresolvers import reverse

__author__ = 'maurizio'

from django.db import models
from oauth2_provider.models import AbstractApplication
from django.utils.translation import ugettext_lazy as _

GRANT_CLIENT_CREDENTIALS = 'client-credentials'
GRANT_TYPES = (
    (GRANT_CLIENT_CREDENTIALS, _('Client credentials')),)


class Tier(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=3000)
    monthly_request_limit = models.IntegerField()

    def __unicode__(self):
        return str(self.name)


class CustomApplication(AbstractApplication):
    activation_data = models.DateField(auto_now_add=True, null=True)
    tier = models.ForeignKey(Tier, null=True, editable=True)
    tier_paid = models.ForeignKey(Tier, null=True, editable=False, related_name='tier_paid', blank=True)
    auto_renewal = models.BooleanField(default=False, editable=False)
    monthly_requests = models.IntegerField(default=0, editable=False)


CustomApplication._meta.get_field('authorization_grant_type').choices = GRANT_TYPES
CustomApplication._meta.get_field('authorization_grant_type').default = GRANT_CLIENT_CREDENTIALS
# CustomApplication._meta.get_field('user').editable = False
CustomApplication._meta.get_field('client_id').editable = False
CustomApplication._meta.get_field('client_secret').editable = False
# CustomApplicationProxy._meta.get_field('tier').editable = True

