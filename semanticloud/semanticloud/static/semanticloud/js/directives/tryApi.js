/**
 * Created by maurizio on 19/08/16.
 */
(function(){
    semantiCloud.directive('tryApi', ['$sce', 'languagesService', '$http', function($sce, languagesService ,$http){
        return {
            restrict: "E",
            link: function(element, attributes, scope){

            },
            controller: function($scope){
                var self = this;
                this.list_of_results = [];
                this.word = "";
                this.languagesService = languagesService;
                this.language = navigator.language || navigator.userLanguage;
                this.type = "n";
                this.operation = 'synonyms';
                this.tryApi = function() {
                    // var csrftoken = Cookies.get('csrftoken');
                    var ladda = Ladda.create( document.querySelector( '.ladda_bind_home') );
                    ladda.start();
                    self.list_of_results.length = 0;
                    $http({
                            method : "POST",
                            url : "/try_api_home/",
                            data: {
                                'operation': this.operation,
                                'word': this.word,
                                'type': this.type,
                                'lang': this.languagesService.get_languages(this.language)
                                // 'csrfmiddlewaretoken': csrftoken
                            }
                        }).then(function mySucces(response) {
                            var data = jQuery.parseJSON(jQuery.parseJSON(response.data));
                            // console.log(data);
                            $.each(data, function(key, val){
                                self.list_of_results.push({"text": val});
                                // application.list_of_results_changed.push({"text": val});
                                // console.log(val);
                            });
                            ladda.stop();
                        }, function myError(response) {
                            alert("Error");
                            ladda.stop();
                        });
                };
            },
            controllerAs: "tryApiCtrl",
            templateUrl: $sce.trustAsResourceUrl("templates/try-api.html")
        };
    }]);
})();