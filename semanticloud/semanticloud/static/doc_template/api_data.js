define({ "api": [
  {
    "type": "post",
    "url": "/o/token/",
    "title": "Obtain the token",
    "name": "GetToken",
    "group": "1_Token",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST -d \"grant_type=client_credentials\" -u\"AKatMNE5WwVNnQC149qvs5K38O2bSVaQkd:HqSUqsliOfKbLxKYUGqsx3pYA9XhTjjlEI6jHVI0TKg7JHFmua3b48ZA9jCPbaEVDshOjRFulzzfUlk0IY7pGyglAMaWmJBKSETUBlj0UMsYsazTgv\" /o/token/",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grant_type",
            "defaultValue": "client_credentials",
            "description": "<p>Default grant type</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\"grant_type=client_credentials\"",
          "type": "String"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Authorization token in the form &quot;&lt;client_id&gt;:&lt;client_secret&gt;&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "\"AKatMNE5WwVNnQC149q5K38O2bSVaQkd:HqSUqsliOfKbmua3b48ZA9jCPbaEVDshOjRsIUBlj0UMsYsazTgv\"",
          "type": "String"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"access_token\": \"mtEvT2AAtRkbFkVxHyb3T4JmGNWeSw\",\n  \"token_type\": \"Bearer\",\n  \"expires_in\": 36000,\n  \"scope\": \"read write\"\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>The access token given by the server</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token_type",
            "description": "<p>Type of token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "expires_in",
            "description": "<p>Expiration time</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "scope",
            "description": "<p>Scope of operations</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidClient",
            "description": "<p>The client is invalid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 InvalidClient\n{\n  \"error\": \"invalid_client\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "core/views.py",
    "groupTitle": "1_Token"
  },
  {
    "type": "get",
    "url": "/api/get_more_general/",
    "title": "More general",
    "name": "GetMoreGeneral",
    "group": "2_Api",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3\" /api/get_more_general/?word=chair&type=n&lang=eng",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "word",
            "description": "<p>Word you want to expand</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Verb, noun or adjective (v, n, a)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Word you want to expand</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "word=chair&type=n&word=chair&lang=eng",
          "type": "String"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Authorization value in the form &quot;Authorization: Bearer &lt;token&gt;&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "\"Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3\"",
          "type": "String"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[berth, office, instrument_of_execution, billet, spot, seat, place, situation, position, post, presiding_officer]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "core/views.py",
    "groupTitle": "2_Api"
  },
  {
    "type": "get",
    "url": "/api/get_more_specific/",
    "title": "More specific",
    "name": "GetMoreSpecific",
    "group": "2_Api",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3\" /api/get_more_specific/?word=chair&type=n&lang=eng",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "word",
            "description": "<p>Word you want to expand</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Verb, noun or adjective (v, n, a)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Word you want to expand</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "word=chair&type=n&word=chair&lang=eng",
          "type": "String"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Authorization value in the form &quot;Authorization: Bearer &lt;token&gt;&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "\"Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3\"",
          "type": "String"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[rocker, daybed, armchair, highchair, tablet-armed_chair, straight_chair, ladder-back, ladder-back_chair, garden_chair, side_chair, chaise_longue, chaise, barber_chair, chair_of_state, Eames_chair, Kalon_Tripa, vice_chairman, swivel_chair, folding_chair, wheelchair, feeding_chair, lawn_chair, fighting_chair]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "core/views.py",
    "groupTitle": "2_Api"
  },
  {
    "type": "get",
    "url": "/api/get_part/",
    "title": "Part",
    "name": "GetPart",
    "group": "2_Api",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3\" /api/get_part/?word=chair&type=n&lang=eng",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "word",
            "description": "<p>Word you want to expand</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Verb, noun or adjective (v, n, a)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Word you want to expand</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "word=chair&type=n&word=chair&lang=eng",
          "type": "String"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Authorization value in the form &quot;Authorization: Bearer &lt;token&gt;&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "\"Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3\"",
          "type": "String"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[leg, back, backrest]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "core/views.py",
    "groupTitle": "2_Api"
  },
  {
    "type": "get",
    "url": "/api/get_synonyms/",
    "title": "Synonyms",
    "name": "GetSynonyms",
    "group": "2_Api",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3\" /api/get_synonyms/?word=chair&type=n&lang=eng",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "word",
            "description": "<p>Word you want to expand</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Verb, noun or adjective (v, n, a)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Word you want to expand</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "word=chair&type=n&word=chair&lang=eng",
          "type": "String"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Authorization value in the form &quot;Authorization: Bearer &lt;token&gt;&quot;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "\"Authorization: Bearer BJCirePZ31gaDCVQ804FNizHkwSSK3\"",
          "type": "String"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[chair, professorship, president, chairman, chairwoman, chairperson, electric_chair, death_chair, hot_seat]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "core/views.py",
    "groupTitle": "2_Api"
  }
] });
