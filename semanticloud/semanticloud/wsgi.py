"""
WSGI config for semanticloud project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
# import sys
from django.core.wsgi import get_wsgi_application

# sys.path.append('/opt/bitnami/apps/django/django_projects/SemantiCloud/semanticloud/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "semanticloud.settings")
# os.environ.setdefault("PYTHON_EGG_CACHE", "/opt/bitnami/apps/django/django_projects/SemantiCloud/semanticloud/semanticloud/egg_cache")

application = get_wsgi_application()
