define({ "api": [
  {
    "type": "post",
    "url": "/o/token/",
    "title": "Obtain the token",
    "name": "GetToken",
    "group": "Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grant_type",
            "defaultValue": "client_credentials",
            "description": "<p>Default grant type</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\"grant_type=client_credentials\"",
          "type": "String"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>Authorization - value in the form client_id:client_secret</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "\"AKatMNE5WwVNnQC149q5K38O2bSVaQkd:HqSUqsliOfKbmua3b48ZA9jCPbaEVDshOjRsIUBlj0UMsYsazTgv\"",
          "type": "String"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"access_token\": \"mtEvT2AAtRkbFkVxHyb3T4JmGNWeSw\",\n  \"token_type\": \"Bearer\",\n  \"expires_in\": 36000,\n  \"scope\": \"read write\"\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>The access token given by the server</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token_type",
            "description": "<p>Type of token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "expires_in",
            "description": "<p>Expiration time</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "scope",
            "description": "<p>Scope of operations</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidClient",
            "description": "<p>The client is invalid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 InvalidClient\n{\n  \"error\": \"invalid_client\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "core/views.py",
    "groupTitle": "Token"
  }
] });
