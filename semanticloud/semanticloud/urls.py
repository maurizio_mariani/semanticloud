"""semanticloud URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Homes
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import TemplateView
from views import logout_user, login_user, welcome, ApplicationRegistration, ApplicationUpdate, TryApi, RaiseForbidden, Pay, StartPayment, UpdateSemantic, \
    TryApiHome, ApplicationList
from django.contrib.auth.views import password_change_done, password_change
from django.views.generic import RedirectView


urlpatterns = [
    url('^$', welcome),
    url(r'^profile', login_required(TemplateView.as_view(template_name='profile.html')), name='profile'),
    url(r'^raise403', RaiseForbidden.as_view(), name='RaiseForbidden'),
    url(r'^home', TemplateView.as_view(template_name='home.html')),
    # url(r'^home', TemplateView.as_view(template_name='base.html')),
    url(r'^doc', TemplateView.as_view(template_name='doc_template/index.html')),
    url(r'^login/$', login_user, name='login'),
    url(r'^logout/$', logout_user),
    url(r'^admin/', admin.site.urls),
    url(r'^pay/', Pay.as_view(), name='pay'),
    url(r'^contacts/', TemplateView.as_view(template_name='contacts.html'), name='contacts'),
    url(r'^howto/$', TemplateView.as_view(template_name='howto.html'), name='howto'),
    url(r'^start_payment/', StartPayment.as_view(), name='StartPayment'),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^o/applications/$', ApplicationList.as_view()),
    url(r'^o/applications/register/$', permission_required('is_staff')(ApplicationRegistration.as_view())),
    url(r'^o/applications/(?P<pk>\d+)/update/$', ApplicationUpdate.as_view(), name="update"),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^api/', include('core.urls')),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    # url(r'^password_change/$', password_change, {'template_name': 'password_change_form.html'}, name='password_change'),
    url(r'^password_change/done/$', RedirectView.as_view(pattern_name='profile'), name='password_change_done'),
    url('^', include('django.contrib.auth.urls')),
    url(r'^try_api/$', TryApi.as_view()),
    url(r'^try_api_home/$', TryApiHome.as_view()),
    url(r'^update_sematic/$', UpdateSemantic.as_view()),
]
