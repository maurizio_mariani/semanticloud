from django.core.validators import RegexValidator

__author__ = 'maurizio'

from django import forms
from datetime import date

numeric = RegexValidator(r'^[0-9]*$', 'Only alphanumeric characters are allowed.')


class PaymentForm(forms.Form):
    FirstName = forms.CharField(label='First name', max_length=100)
    LastName = forms.CharField(label='Last name', max_length=100)
    Month = forms.IntegerField(label='Month', max_value=12, min_value=1)
    # Year = forms.IntegerField(label='Year', max_value=date.today().year + 15, min_value=2011)
    Year = forms.IntegerField(label='Year', max_value=date.today().year + 15, min_value=date.today().year)
    Number = forms.CharField(label='Number', validators=[numeric])
    Verification = forms.CharField(label='Verification', validators=[numeric])