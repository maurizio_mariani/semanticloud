from django.contrib import admin
from models import Tier


class TierAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'monthly_request_limit')
    search_fields = ['name']


class CustomApplicationProxyAdmin(admin.ModelAdmin):
    list_display = ('name', 'monthly_requests', 'client_id', 'client_secret', 'user', 'client_type', 'tier', 'tier_paid', 'activation_data')
    search_fields = ['user', 'client_id']

# admin.site.register(CustomApplicationProxy, CustomApplicationProxyAdmin)
admin.site.register(Tier, TierAdmin)
