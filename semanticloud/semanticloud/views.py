from django.contrib.auth.decorators import user_passes_test
from requests import ConnectionError

__author__ = 'maurizio'

from django.db.models import Q
from billing.utils.credit_card import CardNotSupported
from core.utils import get_lemmas
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie, requires_csrf_token
from setuptools.package_index import unique_everseen
import json
from billing import get_gateway
from billing import CreditCard
from requests_oauthlib import OAuth2Session
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import modelform_factory
from django.views.generic import CreateView, UpdateView, View, ListView
from oauth2_provider.models import get_application_model
from models import Tier
from oauth2_provider.views.application import ApplicationOwnerIsUserMixin
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render_to_response, redirect, render, get_object_or_404
from django.template import RequestContext
from django.utils.datastructures import MultiValueDictKeyError
from django.http import JsonResponse, HttpResponseForbidden, HttpResponseRedirect, HttpResponseNotAllowed
from django.conf import settings
from forms import PaymentForm
from oauthlib.oauth2 import BackendApplicationClient, TokenExpiredError
from django.db import transaction
from core import utils
from core.models import Sense
from models import CustomApplication


def login_user(request):
    next_url = request.POST.get('next') or '/'
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            # applications = CustomApplication.objects.filter(user=user)
            # request.session['applications'] = serializers.serialize("json", applications)
            if next_url is not None:
                return redirect(next_url)
            else:
                return redirect('/')
        else:
            return redirect('login')
    else:
        try:
            context_par = {'next': request.GET['next']}
        except MultiValueDictKeyError:
            context_par = {'next': '/o/applications'}
        return render_to_response('login.html', context=context_par, context_instance=RequestContext(request))


def logout_user(request):
    logout(request)
    return redirect('/home')


def welcome(request):
    if request.user.is_authenticated():
        return redirect('/o/applications')
    else:
        return redirect('/home')


class ApplicationRegistration(LoginRequiredMixin, CreateView):
    """
    View used to register a new Application for the request.user
    """
    template_name = "oauth2_provider/application_registration_form.html"

    def get_form_class(self):
        """
        Returns the form class for the application model
        """
        if self.request.user.is_superuser or self.request.user.is_staff:
            return modelform_factory(
                get_application_model(),
                fields=(
                    # 'user', 'name', 'client_type', 'tier', 'authorization_grant_type', 'redirect_uris', 'auto_renewal')
                    'user', 'name', 'client_type', 'tier', 'authorization_grant_type', 'redirect_uris')
            )
        else:
            return modelform_factory(
                get_application_model(),
                # fields=('name', 'client_type', 'tier', 'authorization_grant_type', 'redirect_uris', 'auto_renewal')
                fields=('name', 'client_type', 'authorization_grant_type', 'redirect_uris')
            )

    @transaction.atomic
    def form_valid(self, form):
        if form.instance.tier is None:
            form.instance.tier = Tier.objects.get(name='Free')
        if form.instance.tier == Tier.objects.get(name='Free') and len(
                get_application_model().objects.filter(user=self.request.user).filter(
                    tier=Tier.objects.get(name='Free'))) > 0:
            # form.errors['tier'] = "You can't have more than one free tier"
            form.errors['name'] = "You can't have more than one free tier"
            print form.errors
            return super(ApplicationRegistration, self).form_invalid(form)
        else:
            if len(get_application_model().objects.filter(user=self.request.user).exclude(
                    tier=Tier.objects.get(name='Free')).filter(
                tier_paid=None)) > 0 and not self.request.user.is_staff and not settings.BETA:
                form.errors[
                    'tier'] = "You have to pay all your previous applications before you can register another one"
                return super(ApplicationRegistration, self).form_invalid(form)
            else:
                with transaction.atomic():
                    if not self.request.user.is_staff and not self.request.user.is_superuser:
                        form.instance.user = self.request.user
                    application = form.save()
                    if application.tier.name == 'Free':
                        return HttpResponseRedirect('/o/applications/')
                    else:
                        self.request.session['app_to_pay'] = application.id
                        self.request.session['tier'] = application.tier.id
                        if settings.BETA:
                            return HttpResponseRedirect('/o/applications/')
                        else:
                            return HttpResponseRedirect('/pay/')


class ApplicationUpdate(LoginRequiredMixin, ApplicationOwnerIsUserMixin, UpdateView):
    """
    View used to update an application owned by the request.user
    """
    context_object_name = 'application'
    template_name = "oauth2_provider/application_form.html"

    def get_form_class(self):
        """
        Returns the form class for the application model
        """
        if self.request.user.is_superuser or self.request.user.is_staff:
            return modelform_factory(
                get_application_model(),
                fields=(
                    # 'user', 'name', 'client_type', 'tier', 'authorization_grant_type', 'redirect_uris', 'auto_renewal')
                    'user', 'name', 'client_type', 'tier', 'authorization_grant_type', 'redirect_uris')
            )
        else:
            return modelform_factory(
                get_application_model(),
                # fields=('name', 'client_type', 'tier', 'authorization_grant_type', 'redirect_uris', 'auto_renewal')
                fields=('name', 'client_type', 'authorization_grant_type', 'redirect_uris')
            )

    @transaction.atomic
    def form_valid(self, form):
        if form.instance.tier is None:
            print "case1"
            form.instance.tier = Tier.objects.get(name='Free')
        if form.instance.tier == Tier.objects.get(name='Free') and len(
                get_application_model().objects.filter(user=self.request.user).filter(
                    tier=Tier.objects.get(name='Free'))) > 1:
            # form.errors['tier'] = "You can't have more than one free tier"
            # print "case2"
            form.errors['name'] = "You can't have more than one free tier"

            return super(ApplicationUpdate, self).form_invalid(form)
        else:
            if len(get_application_model().objects.filter(user=self.request.user).exclude(
                    tier=Tier.objects.get(name='Free')).filter(
                tier_paid=None)) > 1 and not self.request.user.is_staff and not settings.BETA:
                form.errors[
                    'tier'] = "You have to pay all your previous applications before you can register another one"
                # print "case3"
                return super(ApplicationUpdate, self).form_invalid(form)
            else:
                with transaction.atomic():
                    if not self.request.user.is_staff and not self.request.user.is_superuser:
                        # print "case4"
                        form.instance.user = self.request.user
                    application = form.save()
                    if application.tier.name == 'Free':
                        print "case5"
                        return HttpResponseRedirect('/o/applications/')
                    self.request.session['app_to_pay'] = application.id
                    self.request.session['tier'] = application.tier.id
                    if settings.BETA:
                        return HttpResponseRedirect('/o/applications/')
                    else:
                        return HttpResponseRedirect('/pay/')


class ApplicationList(ApplicationOwnerIsUserMixin, ListView):
    """
    List view for all the applications owned by the request.user
    """

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ApplicationList, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['BETA'] = settings.BETA
        return context

    context_object_name = 'applications'
    template_name = "oauth2_provider/application_list.html"


class RaiseForbidden(LoginRequiredMixin, View):
    @staticmethod
    def get(request):
        return HttpResponseForbidden()


class StartPayment(LoginRequiredMixin, View):
    @staticmethod
    def get(request):
        request.session['app_to_pay'] = request.GET['application_id']
        request.session['tier'] = request.GET['tier_id']
        return HttpResponseRedirect('/pay/')


class Pay(LoginRequiredMixin, View):
    template_name = "form_payment.html"
    form_class = PaymentForm

    def get(self, request):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    @transaction.atomic
    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            with transaction.atomic():
                first_name = form.cleaned_data['FirstName']
                last_name = form.cleaned_data['LastName']
                month = form.cleaned_data['Month']
                year = form.cleaned_data['Year']
                number = form.cleaned_data['Number']
                verification = form.cleaned_data['Verification']
                application_id = request.session['app_to_pay']
                tier_id = request.session['tier']
                application = get_application_model().objects.get(pk=application_id)
                tier = Tier.objects.get(pk=tier_id)

                # Payment block
                g1 = get_gateway("authorize_net")
                cc = CreditCard(first_name=str(first_name), last_name=str(last_name), month=int(month), year=int(year),
                                number=number, verification_value=str(verification))
                # cc = CreditCard(first_name="Test", last_name="User", month=10, year=2020, number="4222222222222",
                #                 verification_value="100")
                try:
                    response1 = g1.purchase(1, cc)
                except CardNotSupported:
                    print "**************************"
                    print cc
                    print "**************************"
                    print cc.first_name
                    print cc.last_name
                    print cc.month
                    print cc.year
                    print cc.number
                    print cc.verification_value
                    raise

                # End paymen block

                if response1['status'] == "SUCCESS":
                    del request.session['app_to_pay']
                    del request.session['tier']
                    application.tier_paid = tier
                    application.save()
                    self.template_name = "oauth2_provider/application_detail.html"
                    return HttpResponseRedirect('/o/applications/' + str(application_id) + "/")
                else:
                    print response1
                    form.add_error(None, 'Payment can\'t be processed')
                    return render(request, self.template_name, {'form': form})
        else:
            # form.add_error('FirstName', 'Your payment has been refused')
            return render(request, self.template_name, {'form': form})


class TryApi(LoginRequiredMixin, View):
    apiUrls = {
        'synonyms': '/api/get_synonyms/',
        'more_general': '/api/get_more_general/',
        'more_specific': '/api/get_more_specific/',
        'part': '/api/get_part/'
    }

    def get(self, request):
        operation = request.GET['operation']
        client_id = request.GET['client_id']
        secret = request.GET['client_secret']
        language = request.GET['lang']
        word = request.GET['word']
        word_type = request.GET['type']
        application = get_object_or_404(CustomApplication, client_id=client_id)
        if application.user == request.user:
            client = BackendApplicationClient(client_id=client_id)
            # client.prepare_request_body(scope=['read', 'write'])
            oauth = OAuth2Session(client=client, scope=['read', 'write'])
            # if settings.DEBUG is True:
            import os
            os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
            try:
                token = request.session['oauth_token' + client_id]
                oauth = OAuth2Session(client=client, token=token, scope=['read', 'write'])
                print "Token found in session"
                print token
            except KeyError:
                oauth = OAuth2Session(client=client, scope=['read', 'write'])
                token = oauth.fetch_token(token_url="http://" + request.get_host() + '/o/token/', client_id=client_id,
                                          client_secret=secret)
                request.session['oauth_token' + client_id] = token
                print "Token found in session, retrieve it"
                print token
            try:
                if settings.DEBUG is True:
                    url = "http://" + request.get_host() + self.apiUrls[
                        str(operation)] + "?word=" + word + "&type=" + word_type + "&lang=" + language
                    r = oauth.get(url)
                    print url
                else:
                    url = "http://" + request.get_host() + self.apiUrls[
                        str(operation)] + "?word=" + word + "&type=" + word_type + "&lang=" + language
                    r = oauth.get(url)
            except TokenExpiredError:
                if settings.DEBUG is True:
                    print "Expired, old token is"
                    print request.session['oauth_token' + client_id]
                oauth = OAuth2Session(client_id=client_id, client=client)
                token = oauth.fetch_token(token_url="http://" + request.get_host() + '/o/token/', client_id=client_id,
                                          client_secret=secret)
                request.session['oauth_token' + client_id] = token
                if settings.DEBUG is True:
                    print "New token is"
                    print request.session['oauth_token' + client_id]
                if settings.DEBUG is True:
                    url = "http://" + request.get_host() + self.apiUrls[
                        str(operation)] + "?word=" + word + "&type=" + word_type + "&lang=" + language
                    r = oauth.get(url)
                else:
                    url = "http://" + request.get_host() + self.apiUrls[
                        str(operation)] + "?word=" + word + "&type=" + word_type + "&lang=" + language
                    r = oauth.get(url)
            # print "Status code"
            # print r.status_code
            # print r.content
            if r.status_code == 403:
                del request.session['oauth_token' + client_id]
                print settings.BASE_DIR
                return HttpResponseForbidden()
            return JsonResponse(r.content, safe=False)
        else:
            return HttpResponseForbidden


class TryApiHome(View):
    apiUrls = {
        'synonyms': '/api/get_synonyms/',
        'more_general': '/api/get_more_general/',
        'more_specific': '/api/get_more_specific/',
        'part': '/api/get_part/'
    }

    def post(self, request):
        request_body = json.loads(request.body)
        client_id = settings.HOME_CLIENT_ID
        secret = settings.HOME_CLIENT_SECRET
        application = get_object_or_404(CustomApplication, client_id=client_id)
        operation = request_body['operation']
        language = request_body['lang']
        word = request_body['word']
        word_type = request_body['type']
        client = BackendApplicationClient(client_id=client_id)
        # client.prepare_request_body(scope=['read', 'write'])
        oauth = OAuth2Session(client=client, scope=['read', 'write'])
        # if settings.DEBUG is True:
        import os
        os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
        url = "http://" + request.get_host() + self.apiUrls[
            str(operation)] + "?word=" + word + "&type=" + word_type + "&lang=" + language
        oauth = OAuth2Session(client_id=client_id, client=client)
        token = oauth.fetch_token(token_url="http://" + request.get_host() + '/o/token/', client_id=client_id,
                                      client_secret=secret)
        r = oauth.get(url)
        if r.status_code == 403:
            del request.session['oauth_token' + client_id]
            print settings.BASE_DIR
            return HttpResponseForbidden()
        return JsonResponse(r.content, safe=False)


class UpdateSemantic(LoginRequiredMixin, View):
    @transaction.atomic
    @method_decorator(csrf_protect)
    def post(self, request):

        def __are_synosyms(x, y, w_type, lang, app):
            synsets_x = utils.get_synsets(x, w_type, lang, app)
            for synset_local in synsets_x:
                lemmas_local = utils.get_lemmas(synset_local['code'], app)
                if y in lemmas_local:
                    return True
            return False

        # get_dict = dict(request.GET.iterlists())
        word = request.GET['word']
        word_type = request.GET['word_type']
        application = get_application_model().objects.get(pk=request.GET['application_id'])
        language = request.GET['language']
        synsets = utils.get_synsets(word, word_type, language, application)
        words = []
        synsets_list = []
        for synset in synsets:
            synsets_list.append(synset['code'])
            lemmas = get_lemmas(synset['code'], language, application)
            for lemma in lemmas:
                try:
                    sense = Sense.objects.get(Q(applicationRefID=application), Q(synsetId=synset['code']),
                                              Q(wordId=lemma), Q(language=language))
                    if sense.removed is False:
                        words.append(lemma)
                except ObjectDoesNotExist:
                    words.append(lemma)
                except MultipleObjectsReturned:
                    raise
        words = list(unique_everseen(words))
        tags = []
        word_list = json.loads(request.GET['word_list'])
        for word_item in word_list:
            tags.append(word_item['text'])
        tags = list(unique_everseen(tags))
        print "words:"
        print words
        print "tags:"
        print tags
        for tag in tags:
            if tag not in words:
                print "Adding tag " + tag
                try:
                    sense = Sense.objects.get(Q(wordId=tag), Q(synsetId=synsets[0]['code']))
                    if sense.removed is True:
                        sense.delete()
                except ObjectDoesNotExist:
                    sense = Sense(wordId=tag, synsetId=synsets[0]['code'], language=language,
                                  applicationRefID=application)
                    sense.save()
        """
        TODO: Reverse check, for each word in words check if there is in tags (this control verify if you have delete
        something)
        """
        for word_local in words:
            if word_local not in tags:
                print "need to remove " + word_local
                senses = Sense.objects.filter(wordId=word_local)
                if len(senses) > 0:
                    for local_sense in senses:
                        if local_sense.synsetId in synsets_list and local_sense.removed is False:
                            sense_to_remove = Sense.objects.get(Q(synsetId=local_sense.synsetId),
                                                                Q(wordId=local_sense.wordId))
                            sense_to_remove.delete()
                else:
                    sense_to_add_in_removed = Sense(synsetId=synsets[0]['code'], wordId=word_local,
                                                    applicationRefID=application, language=language, removed=True)
                    sense_to_add_in_removed.save()
        return JsonResponse(word_list, safe=False)
